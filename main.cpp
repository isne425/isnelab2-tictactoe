#include<iostream>

using namespace std;

int win(const int *board) 
{
	// Determines player won -> return 0;
	unsigned wins[8][3] = { { 0,1,2 },{ 3,4,5 },{ 6,7,8 },{ 0,3,6 },{ 1,4,7 },{ 2,5,8 },{ 0,4,8 },{ 2,4,6 } }; // winning positions
	for (int i = 0; i < 8; ++i) 
	{
		if (board[wins[i][0]] != 0 &&
			board[wins[i][0]] == board[wins[i][1]] &&
			board[wins[i][1]] == board[wins[i][2]])
		{
			return board[wins[i][2]];
		}
	}
	return 0;
}

int minimax(int *board, int player) // computer decission
{
	int winner = win(board);
	if (winner != 0){return winner*player;}
	int move = -1;
	int score = -2;
	for (int i = 0; i < 9; i++) 
	{
		if (board[i] == 0) // empty 
		{
			board[i] = player;
			int thisScore = -minimax(board, player*-1);
			if (thisScore > score) 
			{
				score = thisScore;
				move = i;
			}
			board[i] = 0;//Reset board after trying
		}
	}
	if (move == -1){return 0;}
	return score;
}


int player2(int *board) { // computer movement
	int move = -1;
	int score = -2;

	for (int i = 0; i < 9; ++i) {

		if (board[i] == 0) {
			board[i] = 1;
			int tempScore = -minimax(board, -1);
			board[i] = 0;

			if (tempScore > score) {
				score = tempScore;
				move = i;
			}
		}
	}
	//returns a score based on minimax tree at a given node.
	return move;
}

void draw(int *b) 
{
	int k = 0; // box number
	cout << "---------------" << endl << endl;
	for (int i = 0; i<3; i++) {
		for (int j = 0; j<3; j++) {
			cout << "| ";
			if (b[k] == 0) {  // unselected box
				cout << k + 1 << " |";
			}
			else {
				if (b[k] == -1) { // selected by player
					cout << "X |";
				}
				else { // selected by computer
					cout << "O |";
				}
			}
			k++;
		}
		cout << endl;
		cout << "---------------" << endl << endl;
	}
}




int main()
{
	cout << "Tic Tac Toe" << endl << endl;
	cout << "---------------" << endl;
	cout << "| 1 || 2 || 3 |" << endl;
	cout << "---------------" << endl;
	cout << "| 4 || 5 || 6 |" << endl;
	cout << "---------------" << endl;
	cout << "| 7 || 8 || 9 |" << endl;
	cout << "---------------" << endl << endl << endl;


	int board[9] = { 0 }; // hold value in each box
	int moves = 0, k;
	//Player = -1 ; Bot = 1
	while (moves < 9) // there are some empty boxes
	{
		int mv;
		cout << "Your turn : "; 
		cin >> mv;
		cout << endl << endl;

		if (board[mv - 1] == 0) // box is empty
		{
			board[mv - 1] = -1; // player is select this box
			moves++;
			cout << endl;
			cout << ">>>>> You <<<<<" << endl;
			draw(board);
			if (win(board) == 0) // player isn't win yet --> computer's turn
			{
				k = player2(board);
				board[k] = 1;
				cout << endl;
				cout << ">>> Computer <<<" << endl;
				draw(board);
				moves++;
				if (win(board) != 0) {break;}
			}
			else // player win
			{
				break;
			}
		}
		else  // player choosed full box
		{
			cout << "Invalid movement." << endl << endl;
		}
	}
	if (win(board) == 0) {
		cout << "Tie !" << endl;
	}
	if (win(board) == 1) {
		cout << "You lose ;)" << endl;
	}
	if (win(board) == -1) {
		cout << "Impossible. You win!!" << endl;
	}
	cout << endl;

	system("PAUSE");

	return 0;
}